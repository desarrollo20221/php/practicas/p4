<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            *{
                margin:0px;
                padding: 0px;
            }
            
            span{
                width: 10px;
                height: 50px;
                background-color: #001ee7;
                display: inline-block;
            }
            
            div{
                height: 50px;
                display: inline-block;
                border: 5px solid #ccc;
                margin: 20px;
            }
        </style>
    </head>
    <body>
        <?php
        // inicializas el array $datos 
        $datos = [20, 40, 790, 234, 890];
        $todo = max($datos);// devuelve el dato mas grande
        
        // recorres datos
        foreach ($datos as $key => $value){
            $datos[$key] = (int) (100 * $datos[$key] / $todo);
        }
        var_dump($datos);// imformacion de $datos
        
        //recorres datos para que para cada posicion coloque las etiquetas HTML
        foreach ($datos as $value){
            echo "<div>";
            for ($c=0; $c < $value; $c++){
                echo "<span></span>";
            }
            echo '</div>';
            echo '<br>';
        }
        ?>
    </body>
</html>
